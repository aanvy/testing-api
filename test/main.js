process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);
describe('Main', () => {
let authToken = undefined;
  describe('/', () => {
      it('it should GET main response', (done) => {
        chai.request(server)
            .get('/')
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('object');
                  authToken = res.body.token;
              done();
            });
      });
  });
  describe('/', () => {
    it('token value is not undefined', (done) => {
      should.not.equal(authToken, undefined);
      done();
    });
  });
  describe('/users', () => {
    it('Get users', (done) => {
      chai.request(server)
          .get('/users')
          .set('Authorization', `Bearer ${authToken}`)
          .end((err, res) => {
                res.should.have.status(200);
            done();
          });
    });
  }); 
  describe('/users', () => {
    it('Get users', (done) => {
      chai.request(server)
          .get('/users')
          .end((err, res) => {
                res.should.have.status(401);
            done();
          });
    });
  });  
});