let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let port = 8080;
let users = [
    {
        name: 'a'
    },
    {
        name: 'b'
    }
];
//parse application/json and look for raw text                                        
app.use(bodyParser.json());                                     
app.use(bodyParser.urlencoded({extended: true}));               
app.use(bodyParser.text());                                    
app.use(bodyParser.json({ type: 'application/json'}));  

let token = 'eyJhbGciOiJIUzUxMiJ9.eyJvcmdhbml6YXRpb25JZCI6MSwic3ViIjoidmluYXlAYXJtb3MuaW8iLCJyb2xlSWQiOjEsImlkIjoxLCJleHAiOjE1NDU1Njk1MzEsImlhdCI6MTU0NTU2NTkzMX0.lFvIRo0cnOVBptZL8qvGZAYMRtOWckBHE9IbabV3r74B6bkEJNMfxp7jhw1oS5a45JKzD1lo4Joe6_vGg2lR0g';
app.get("/", (req, res) => res.json({token}));

app.get("/users", (req, res) => {
    if(req.headers.authorization === 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJvcmdhbml6YXRpb25JZCI6MSwic3ViIjoidmluYXlAYXJtb3MuaW8iLCJyb2xlSWQiOjEsImlkIjoxLCJleHAiOjE1NDU1Njk1MzEsImlhdCI6MTU0NTU2NTkzMX0.lFvIRo0cnOVBptZL8qvGZAYMRtOWckBHE9IbabV3r74B6bkEJNMfxp7jhw1oS5a45JKzD1lo4Joe6_vGg2lR0g') {
        return res.json(users);
    }else {
        return res.sendStatus(401);
    }
    
});

app.listen(port);
console.log("Listening on port " + port);

module.exports = app; // for testing